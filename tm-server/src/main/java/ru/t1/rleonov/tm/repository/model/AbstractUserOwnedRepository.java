package ru.t1.rleonov.tm.repository.model;

import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.rleonov.tm.model.AbstractUserOwnedModel;

@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

}
