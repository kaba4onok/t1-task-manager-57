package ru.t1.rleonov.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.rleonov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.rleonov.tm.api.service.model.IUserOwnedService;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.model.AbstractUserOwnedModel;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel,
        R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R>
        implements IUserOwnedService<M> {

    @NotNull
    protected String getSortType(@Nullable final Sort sort) {
        if (sort == Sort.BY_CREATED) return "created";
        if (sort == Sort.BY_STATUS) return "status";
        else return "name";
    }

}
