package ru.t1.rleonov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.rleonov.tm.model.AbstractUserOwnedModel;
import javax.persistence.EntityManager;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @NotNull
    IUserOwnedRepository<M> getRepository();

}
