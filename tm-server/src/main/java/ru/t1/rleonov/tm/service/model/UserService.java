package ru.t1.rleonov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.rleonov.tm.api.repository.model.IUserRepository;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.api.service.model.IProjectService;
import ru.t1.rleonov.tm.api.service.model.ITaskService;
import ru.t1.rleonov.tm.api.service.model.IUserService;
import ru.t1.rleonov.tm.exception.entity.UserNotFoundException;
import ru.t1.rleonov.tm.exception.field.EmailEmptyException;
import ru.t1.rleonov.tm.exception.field.IdEmptyException;
import ru.t1.rleonov.tm.exception.field.LoginEmptyException;
import ru.t1.rleonov.tm.exception.field.PasswordEmptyException;
import ru.t1.rleonov.tm.exception.user.LoginExistsException;
import ru.t1.rleonov.tm.model.User;
import ru.t1.rleonov.tm.repository.model.UserRepository;
import ru.t1.rleonov.tm.util.HashUtil;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Override
    public UserRepository getRepository() {
        return context.getBean(UserRepository.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
        if (hashPassword == null || hashPassword.isEmpty()) throw new PasswordEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPassword(hashPassword);
        user.setEmail(email);
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPassword(HashUtil.salt(propertyService, password));
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    @SneakyThrows
    public void set(@NotNull final Collection<User> users) {
        if (users.isEmpty()) return;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.set(users);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
