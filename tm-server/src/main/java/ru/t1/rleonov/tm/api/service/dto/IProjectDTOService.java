package ru.t1.rleonov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.enumerated.Status;
import java.util.List;

public interface IProjectDTOService extends IUserOwnedDTOService<ProjectDTO> {

    @NotNull
    IProjectDTORepository getRepository();

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    ProjectDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    ProjectDTO removeById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    ProjectDTO changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    void clear(@Nullable String userId);

    void clear();

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<ProjectDTO> findAll();

    @Nullable
    ProjectDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    );

}
