package ru.t1.rleonov.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import ru.t1.rleonov.tm.dto.model.SessionDTO;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import ru.t1.rleonov.tm.dto.model.UserDTO;
import ru.t1.rleonov.tm.model.Project;
import ru.t1.rleonov.tm.model.Session;
import ru.t1.rleonov.tm.model.Task;
import ru.t1.rleonov.tm.model.User;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;
import static org.hibernate.cfg.AvailableSettings.*;

@Configuration
@ComponentScan("ru.t1.rleonov.tm")
public class ServerConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory getEntityManagerFactory(@NotNull final IPropertyService propertyService) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(DRIVER, propertyService.getDbDriver());
        settings.put(URL, propertyService.getDbUrl());
        settings.put(USER, propertyService.getDbLogin());
        settings.put(PASS, propertyService.getDbPassword());
        settings.put(DIALECT, propertyService.getDbDialect());
        settings.put(DEFAULT_SCHEMA, propertyService.getDbSchema());
        settings.put(HBM2DDL_AUTO, propertyService.getDbDdlAuto());
        settings.put(SHOW_SQL, propertyService.getDbShowSql());
        settings.put(FORMAT_SQL, propertyService.getDbShowSql());
        settings.put(USE_SECOND_LEVEL_CACHE, propertyService.getDbSecondLvlCash());
        settings.put(CACHE_REGION_FACTORY, propertyService.getDbFactoryClass());
        settings.put(USE_QUERY_CACHE, propertyService.getDbUseQueryCash());
        settings.put(USE_MINIMAL_PUTS, propertyService.getDbUseMinPuts());
        settings.put(CACHE_REGION_PREFIX, propertyService.getDbRegionPrefix());
        settings.put(CACHE_PROVIDER_CONFIG, propertyService.getDbHazelConfig());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}
