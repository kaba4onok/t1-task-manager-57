package ru.t1.rleonov.tm.listener;

public enum OperationType {

    INSERT,
    DELETE,
    UPDATE

}
