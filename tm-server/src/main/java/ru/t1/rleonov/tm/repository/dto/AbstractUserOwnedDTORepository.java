package ru.t1.rleonov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.rleonov.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<M>
        implements IUserOwnedDTORepository<M> {

}
