package ru.t1.rleonov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.model.AbstractModel;
import javax.persistence.EntityManager;
import java.util.Collection;

public interface IRepository<M extends AbstractModel> {

    void add(@NotNull M model);

    void set(@NotNull Collection<M> models);

    void update(@NotNull M model);

    void remove(@NotNull M model);

    @NotNull
    EntityManager getEntityManager();

}
