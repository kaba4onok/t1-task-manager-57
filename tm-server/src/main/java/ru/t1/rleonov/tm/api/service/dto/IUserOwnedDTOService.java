package ru.t1.rleonov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.rleonov.tm.dto.model.AbstractUserOwnedModelDTO;


public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedDTORepository<M>, IDTOService<M> {

    @NotNull
    IUserOwnedDTORepository<M> getRepository();

}
