package ru.t1.rleonov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.api.endpoint.*;
import ru.t1.rleonov.tm.api.service.*;
import ru.t1.rleonov.tm.exception.system.CommandNotSupportedException;
import ru.t1.rleonov.tm.listener.AbstractListener;
import ru.t1.rleonov.tm.event.ConsoleEvent;
import ru.t1.rleonov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.rleonov.tm.util.SystemUtil;
import ru.t1.rleonov.tm.util.TerminalUtil;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                commandVerify(command);
                loggerService.command(command);
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void commandVerify(@Nullable final String command) {
        if (command == null) throw new CommandNotSupportedException();
        listeners
                .stream()
                .filter(listener -> listener.getName().equals(command))
                .findFirst()
                .orElseThrow(CommandNotSupportedException::new);
    }

    private void processArguments(@NotNull final String[] args) {
        @NotNull final String arg = args[0];
        try {
            processArgument(arg);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
        }
    }

    private void processArgument(@Nullable final String arg) {
        if (arg == null) throw new ArgumentNotSupportedException();
        @NotNull final AbstractListener listener = listeners
                .stream()
                .filter(l -> l.getArgument().equals(arg))
                .findFirst()
                .orElseThrow(ArgumentNotSupportedException::new);
        publisher.publishEvent(new ConsoleEvent(listener.getName()));
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("***WELCOME TO TASK-MANAGER***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("***TASK-MANAGER IS SHUTTING DOWN***");
    }

    public void run(@Nullable final String[] args) {
        prepareStartup();
        if (args == null || args.length == 0) {
            processCommands();
            return;
        }
        processArguments(args);
    }

}
