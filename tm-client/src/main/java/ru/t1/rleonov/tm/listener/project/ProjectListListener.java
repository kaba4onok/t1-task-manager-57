package ru.t1.rleonov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.ProjectListRequest;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import ru.t1.rleonov.tm.event.ConsoleEvent;
import ru.t1.rleonov.tm.util.TerminalUtil;
import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-list";

    @NotNull
    private static final String DESCRIPTION = "Show project list.";

    @Override
    @EventListener(condition = "@projectListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSort(sort);
        @Nullable final List<ProjectDTO> projects = getProjectEndpoint().getProjectList(request).getProjects();
        if (projects == null) return;
        int index = 1;
        for (@Nullable final ProjectDTO project: projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
