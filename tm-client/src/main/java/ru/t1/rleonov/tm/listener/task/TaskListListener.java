package ru.t1.rleonov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.TaskListRequest;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import ru.t1.rleonov.tm.event.ConsoleEvent;
import ru.t1.rleonov.tm.util.TerminalUtil;
import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-list";

    @NotNull
    private static final String DESCRIPTION = "Show task list.";

    @Override
    @EventListener(condition = "@taskListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sort);
        @Nullable final List<TaskDTO> tasks = getTaskEndpoint().getTaskList(request).getTasks();
        if (tasks == null) return;
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
