package ru.t1.rleonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.ServerLoadDataXmlFasterXmlRequest;
import ru.t1.rleonov.tm.event.ConsoleEvent;

@Component
public final class DataXmlLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-load-xml-fasterxml";

    @NotNull
    private static final String DESCRIPTION = "Load data from xml file using fasterxml.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlLoadFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final ServerLoadDataXmlFasterXmlRequest request = new ServerLoadDataXmlFasterXmlRequest(getToken());
        getDomainEndpoint().loadDataXmlFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
